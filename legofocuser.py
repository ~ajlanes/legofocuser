#!/usr/bin/env python3
import sys
from pathlib import Path
import random
sys.path.insert(0, str(Path.cwd().parent))
from pyindi.device import device

from buildhat import Motor
from buildhat.motors import MotorRunmode

"""
This file uses a skeleton xml file to initialize and
define properties. Similar to this example at indilib
https://www.indilib.org/developers/driver-howto.html#h2-properties
"""


class LegoFocuser(device):

    offset = 0
    speed = 50
    direction = 1
    flip = 1
    
    def ISGetProperties(self, device=None):
        """Property Definiations are generated
        by initProperties and buildSkeleton. No
        need to do it here. """
        pass

    def initProperties(self):
        """Build the vector properties from
        the skeleton file."""
        self.buildSkeleton("legofocuser.xml")

    def ISNewText(self, device, name, names, values):
        """A new text vector has been updated from 
        the client. In this case we update the text
        vector with the IUUpdate function. In a real
        device driver you would probably want to do 
        something more than that. 

        This function is always called by the 
        mainloop
        """

        # DEVICE_PORT - the port on the Build HAT to which the
        # motor is connected.
        
        if name == "DEVICE_PORT":
            try:
                port = self.IUUpdate(device, name, names, values)
                port.state='Ok'
                port["PORT"].value = port["PORT"].value.strip()
                self.IDSet(port)
            except Exception as error:
                self.IDMessage(f"DEVICE_PORT error: {error}")
        
        self.IUUpdate(device, name, names, values, Set=True)

    def ISNewNumber(self, device, name, names, values):

        """A number vector has been updated from the client.
        """

        # FOCUS_SPEED - the absolute speed at which the focuser travels
        
        if name == "FOCUS_SPEED":

            try:
                # update our idea of speed to what the client said
                
                focus = self.IUUpdate(device, name, names, values)

                self.update_speed()

                # No exceptions, so set the status to OK

                focus.state='Ok'
                self.IDSet(focus)
                
            except Exception as error:
                self.IDMessage(f"FOCUS_SPEED error: {error}")

                # Try to set the status to Alert (red)

                focus.state='Alert'
                self.IDSet(focus)

                raise
        
        # REL_FOCUS_POSITION - request a focuser move by
        # FOCUS_RELATIVE_POSITION ticks
            
        if name == "REL_FOCUS_POSITION":
            try:

                # Update our idea of the request

                focpos = self.IUUpdate(device, name, names, values)
                focposval = focpos["FOCUS_RELATIVE_POSITION"].value

                self.IDMessage(f"Request to focus relative by {focposval}")
                
                # Ticks are degrees for us.
                
                self.motor.run_for_degrees(focposval,
                                           speed = self.speed * self.direction * self.flip,
                                           blocking=False)

                # We now need to set the state to Busy until the motor
                # stops running.

                focpos.state='Busy'

                # And set a callback to return to OK when the motor
                # has finished

                self.IEAddTimer(100, self.checkmotoridle)
                
            except Exception as error:
                self.IDMessage(f"IUUpdate error: {error}")
                raise

        # ABS_FOCUS_POSITION - request a focuser move to
        # FOCUS_ABSOLUTE_POSITION ticks

        if name == "ABS_FOCUS_POSITION":
            try:

                # Update our idea of the request

                focpos = self.IUUpdate(device, name, names, values)
                focposval = focpos["FOCUS_ABSOLUTE_POSITION"].value

                # both focposval and curpos are in real units, rather
                # than what is reported by the motor.

                curpos = self.get_position()

                self.IDMessage(f"Moving from {curpos} to {focposval}")
                
                # we move to the new position

                self.motor.run_for_degrees(focposval-curpos,
                                           speed = self.speed * self.direction * self.flip,
                                           blocking=False)
                
                # We now need to set the state to Busy until the motor
                # stops running.

                focpos.state='Busy'

                # And set a callback to return to OK when the motor
                # has finished

                self.IEAddTimer(100, self.checkmotoridle)

            except Exception as error:
                self.IDMessage(f"IUUpdate error: {error}")
                raise

        if name == "FOCUS_SYNC":

            try:
                focus = self.IUUpdate(device, name, names, values)
                syncvalue = focus["FOCUS_SYNC_VALUE"].value
                curpos = self.motor.get_position()
                self.offset = syncvalue-curpos
            except Exception as error:
                self.IDMessage(f"FOCUS_SYNC error: {error}")
                raise
            
        if name == "FOCUS_TIMER":

            try:
                focus = self.IUUpdate(device, name, names, values)
                if focus["FOCUS_TIMER_VALUE"].value != 0:
                    motor.run_for_seconds(1)
                    # focus["FOCUS_TIMER_VALUE"].value / 1000)
            except Exception as error:
                self.IDMessage(f"IUUpdate error: {error}")
                raise
                                          
    
    def ISNewSwitch(self, device, name, names, values):

        """A numer switch has been updated from the client.
        This function handles when a new switch
        
        This function is always called by the 
        mainloop
        """

        if name == "CONNECTION":

            try:
                conn = self.IUUpdate(device, name, names, values)
                if conn["CONNECT"].value == 'Off':
                    conn.state = "Idle"
                    self._is_connected = False
                else:
                    conn.state = "Busy"
                    self.IDSet(conn)
                    prt = self.IUFind("DEVICE_PORT")
                    portval = prt["PORT"].value.strip()
                    self.motor = Motor(portval)
                    conn.state = "Ok"
                    self._is_connected = True
                self.IDSet(conn)

            except Exception as error:
                self.IDMessage(f"IUUpdate error: {error}")
                raise
        if name == "FOCUS_REVERSE_MOTION":
            try:
                focus = self.IUUpdate(device, name, names, values, Set=True)

                self.update_direction()
            except Exception as error:
                self.IDMessage(f"FOCUS_REVERSE_MOTION error: {error}")
                focus.state='Alert'
                self.IDSet(focus)
                
        if name == "FOCUS_MOTION":
            try:
                focus = self.IUUpdate(device, name, names, values, Set=True)

                self.update_direction()

                # No exceptions, so set the status to OK

                focus.state='Ok'
                self.IDSet(focus)
                
            except Exception as error:
                self.IDMessage(f"FOCUS_MOTION error: {error}")

                # Try to set the status to Alert (red)

                focus.state='Alert'
                self.IDSet(focus)

        if name == "FOCUS_ABORT_MOTION":
            try:
                self.motor.stop()
                fam = self.IUFind("FOCUS_ABORT_MOTION")
                fam["ABORT"].value = "Off"
                fam.state = "Ok"
                self.IDSet(fam)
            except Exception as error:
                self.IDMessage(f"FOCUS_ABORT_MOTION error: {error}")

                fam.state='Alert'
                self.IDSet(fam)
                
    def checkmotoridle(self):
        afocpos = self.IUFind("ABS_FOCUS_POSITION")
        afocpos["FOCUS_ABSOLUTE_POSITION"].value = self.get_position()
        if self.motor._runmode == MotorRunmode.NONE:
            focpos = self.IUFind("REL_FOCUS_POSITION")
            focpos.state = "Ok"
            self.IDSet(focpos)
            afocpos.state = "Ok"
        else:
            self.IEAddTimer(100, self.checkmotoridle)
        self.IDSet(afocpos)

    def get_position(self):
        # Get the motor position, accounting for offset
        curpos = self.motor.get_position()
        self.IDMessage(f"position is {curpos} and offset is {self.offset}")
        return curpos+self.offset
        
    def run_until_resistance(self):

        # run motor until it appears it encounters resistance
        # returns number of degrees travelled

        # Initial absolute position of motor

        ipos = self.motor.get_aposition()
        pos = ipos
        revs = 0
        
        while abs(pos-ipos) < 15:
            self.motor.run_for_degrees(360)
            self.motor.run_to_position(ipos)
            pos = self.get_position()
            revs += 1
            
        return revs
            
    def update_speed(self):
        
        # Absolute speed

        self.speed = int(self.IUFind("FOCUS_SPEED")["FOCUS_SPEED_VALUE"].value)

        self.IDMessage(f"Updating speed to {speed}")

    def update_direction(self):
        
        # since the Build HAT API uses positive/negative speed
        # for direction, pull out the direction of focus motion
                
        direction = 1 if (self.IUFind("FOCUS_MOTION"))["FOCUS_INWARD"].value == 'On' else -1

        # And flip if the INDI focus reverse switch is set

        if (self.IUFind("FOCUS_REVERSE_MOTION"))["ENABLED"].value == 'On':
            direction *= -1
            self.flip = -1
        else:
            self.flip = 1


lf = LegoFocuser()
lf.start()
